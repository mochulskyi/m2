/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'uiComponent',
    'Global4net_Cart/js/model/productData'
], function (Component, productData) {
    'use strict';

    return Component.extend({
        initialize: function () {
            this._super();
            this.product = productData
        }
    });
});
