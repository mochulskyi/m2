define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'Magento_Customer/js/customer-data',
    'Global4net_Cart/js/model/productData'
], function ($, modal, customerData, productData) {

   return function (config) {
       var modalContent = $(config.selectorModal);
       var cartData = customerData.get('cart');
       var cartModal = modal({
           'type': 'popup',
           'title': 'Naglowek Dupa',
           'responsive': true,
           'buttons': []
       }, modalContent);
       var findProductById = function (products, id) {
           var product = null;

           products.some(function (item) {
               if (item.product_id === id) {
                   product = item;

                   return true;
               }

               return false;
           })

           return product;
       };

       cartData.subscribe(function (newCartData) {
           if (newCartData.productLastAddedId) {
               cartModal.openModal();
               productData(findProductById(newCartData.items, newCartData.productLastAddedId));
           }
       })

       productData.subscribe(function (newData) {
           console.log(newData);
       })
   }
});
