<?php

namespace Global4net\Cart\Plugin;

use Magento\Checkout\CustomerData\Cart as CoreCart;
use Magento\Checkout\Model\Session;

class Cart
{
    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * Cart constructor.
     * @param Session $checkoutSession
     */
    public function __construct(Session $checkoutSession)
    {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @param CoreCart $subject
     * @param $result
     * @return array
     */
    public function afterGetSectionData(CoreCart $subject, $result)
    {
        $result['productLastAddedId'] = $this->checkoutSession->getLastAddedProductId(true);

        return $result;
    }
}
